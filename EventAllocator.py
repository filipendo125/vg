import heapq
from random import randint
from Event import Event

'''
 Class responsible for holding all of the events
 and performing actions on the list of all events
'''
class EventAllocator():
    idNumber = 1
    listOfEvents = []
    listOfUsedCoordinates = []

    '''
        Class initializer.

        Generates unique coordinates;
        Stores the unique coordinates,the minimum price of all of the tickets and the id number
    '''
    def __init__(self,xRange,yRange):
        numberOfEvents = randint(0,(2*xRange+1)*(2*xRange+1))
        for eventIndicator in range(0,numberOfEvents):

            #Amount of tickets generator. If no tickets created, ignore the event.
            numberOfTickets = randint(0,15)
            if (numberOfTickets !=0):
                xCoordinate = randint(-xRange,xRange)
                yCoordinate = randint(-yRange,yRange)

                # unique coordinates checking
                while ((xCoordinate,yCoordinate) in self.listOfUsedCoordinates):
                    xCoordinate = randint(-xRange,xRange)
                    yCoordinate = randint(-yRange,yRange)
                self.listOfUsedCoordinates.append((xCoordinate,yCoordinate))

                self.listOfEvents.append(Event(self.idNumber,numberOfTickets, xCoordinate,yCoordinate))
                self.idNumber += 1

    '''
        Priority Queue implementation of finding TOP 5 elements.

        First 5 elements are added initially to the heap. Every element added
        afterwards gets compared with the first element of the max-distance Priority Queue.

        First element of the Priority Queue is an event with the highest distance from initial coordinates.

        RETURNS: TOP 5 events according to the distance, or if there are less than five elements - all of them.
    '''

    def list_closest_events(self,position):
        h = []
        for event in self.listOfEvents:
            distance = abs(position[0] - event.coordinates[0]) + abs(position[1] - event.coordinates[1])
            #first five events
            if len(h) < 5:
                heapq.heappush(h,(-distance,event))
            # events closer to the source point than the one stored
            elif (distance < -h[0][0]):
                heapq.heappushpop(h,(-distance,event))
        return h
