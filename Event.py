from random import randint
import sys

'''
    Class responsible for holding event specific information
'''

class Event:
    minPrice = sys.maxint
    id = -1
    coordinates = [0,0]

    '''
        Class initializer. Takes a minimum of all of the ticket prices,
        and stores it together with event id number, and event coordinates
    '''
    def __init__(self,idNumber,numberOfTickets, xCoordinate,yCoordinate):
        for ticketNo in xrange(0,numberOfTickets):
            price = randint(0,20000)
            if price < self.minPrice:
                self.minPrice = price
        self.id = idNumber
        self.coordinates = [xCoordinate,yCoordinate]

    '''
        Prints summary of a specific event
    '''
    def print_summary(self):
        print "Event " + str(self.id) +" Coordinates: (" + str(self.coordinates[0])+","+str(self.coordinates[1]) + ") Price: " + str(self.minPrice)
