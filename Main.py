from random import seed,randint
from EventAllocator import *


'''
    Asks for a user input and extracts the X,Y coordinates

    RETURNS: X,Y coordinates
'''
def inputObtainer():
    inputObtained = raw_input("Please Input Coordinates: \n> ")
    inputObtained = inputObtained.strip().split(',')
    return int(inputObtained[0].strip()),int(inputObtained[1].strip())

'''
    Main method that returns the top 5 ( if more than 5 events presents)
    events. If less than 5 events, returns all of them.

    If no events have been created, this indicates an unfortunate case, so re-run
'''
def main():
    seed()
    pointX,pointY = inputObtainer()

    # input coordinates boundary checking
    while (pointX > 10 or pointX < -10 or pointY > 10 or pointY < -10):
        print ("Unfortunately. You provided wrong input.Try again\n")
        pointX,pointY = inputObtainer()

    grid = EventAllocator(10,10)
    # At least one event was created
    if (len(grid.listOfEvents) > 0):
        closestEvents= grid.list_closest_events([pointX,pointY])
        for event in closestEvents:
            print "Event %03d - $%d.%d, Distance %d" % (event[1].id,event[1].minPrice/100,event[1].minPrice%100,-event[0])
    # No events created
    else:
        print "No events added ( Seed value). Re-run!"


if __name__ == '__main__':
    main()
